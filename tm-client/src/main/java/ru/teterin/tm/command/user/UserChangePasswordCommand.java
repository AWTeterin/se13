package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.api.endpoint.User;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.util.PasswordHashUtil;

public final class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change user password.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.CHANGE_PASSWORD);
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_NEW_PASSWORD);
        @Nullable String newPassword = terminalService.readString();
        userEndpoint = serviceLocator.getUserEndpoint();
        @NotNull final User user = new User();
        user.setUserId("2");
        user.setLogin(stateService.getLogin());
        newPassword = PasswordHashUtil.md5(newPassword);
        user.setPassword(newPassword);
        userEndpoint.mergeUser(session, user);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}

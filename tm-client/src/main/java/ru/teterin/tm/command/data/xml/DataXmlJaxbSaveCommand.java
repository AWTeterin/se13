package ru.teterin.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Role;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class DataXmlJaxbSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-xml-jb-save";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "The save of the subject area in the transport format XML with the use JAX-B.";
    }

    @Override
    public void execute() throws Exception {
        terminalService.print(Constant.DATA_XML_JB_SAVE);
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        domainEndpoint = serviceLocator.getDomainEndpoint();
        domainEndpoint.saveXmlJaxb(session);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public Role[] roles() {
        @NotNull final Role[] onlyAdmin = new Role[]{Role.ADMIN};
        return onlyAdmin;
    }

}

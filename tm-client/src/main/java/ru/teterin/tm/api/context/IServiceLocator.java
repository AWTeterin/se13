package ru.teterin.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    public ISessionEndpoint getSessionEndpoint();

    @NotNull
    public IUserEndpoint getUserEndpoint();

    @NotNull
    public IProjectEndpoint getProjectEndpoint();

    @NotNull
    public ITaskEndpoint getTaskEndpoint();

    @NotNull
    public IDomainEndpoint getDomainEndpoint();

}

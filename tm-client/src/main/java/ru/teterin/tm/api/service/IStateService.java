package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.command.AbstractCommand;

import java.util.Map;

public interface IStateService {

    @Nullable
    public String getSession();

    public void setSession(
        @Nullable final String session
    );

    @Nullable
    public String getLogin();

    public void setLogin(
        @Nullable final String login
    );

    @NotNull
    public AbstractCommand getCommand(
        @Nullable final String commandName
    );

    public void registryCommand(
        @NotNull final AbstractCommand command
    );

    @NotNull
    public Map<String, AbstractCommand> getCommands();

}

package ru.teterin.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-06-15T17:29:10.860+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.api.tm.teterin.ru/", name = "IUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface IUserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/loginIsFreeRequest", output = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/loginIsFreeResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/loginIsFree/Fault/Exception")})
    @RequestWrapper(localName = "loginIsFree", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.LoginIsFree")
    @ResponseWrapper(localName = "loginIsFreeResponse", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.LoginIsFreeResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean loginIsFree(
        @WebParam(name = "login", targetNamespace = "")
            java.lang.String login
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/mergeUserRequest", output = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/mergeUserResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/mergeUser/Fault/Exception")})
    @RequestWrapper(localName = "mergeUser", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.MergeUser")
    @ResponseWrapper(localName = "mergeUserResponse", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.MergeUserResponse")
    public void mergeUser(
        @WebParam(name = "session", targetNamespace = "")
            java.lang.String session,
        @WebParam(name = "user", targetNamespace = "")
            ru.teterin.tm.api.endpoint.User user
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/persistUserRequest", output = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/persistUserResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/persistUser/Fault/Exception")})
    @RequestWrapper(localName = "persistUser", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.PersistUser")
    @ResponseWrapper(localName = "persistUserResponse", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.PersistUserResponse")
    public void persistUser(
        @WebParam(name = "user", targetNamespace = "")
            ru.teterin.tm.api.endpoint.User user
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/findOneUserRequest", output = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/findOneUserResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/findOneUser/Fault/Exception")})
    @RequestWrapper(localName = "findOneUser", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.FindOneUser")
    @ResponseWrapper(localName = "findOneUserResponse", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.FindOneUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.teterin.tm.api.endpoint.User findOneUser(
        @WebParam(name = "session", targetNamespace = "")
            java.lang.String session,
        @WebParam(name = "userId", targetNamespace = "")
            java.lang.String userId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/removeUserRequest", output = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/removeUserResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.teterin.ru/IUserEndpoint/removeUser/Fault/Exception")})
    @RequestWrapper(localName = "removeUser", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.RemoveUser")
    @ResponseWrapper(localName = "removeUserResponse", targetNamespace = "http://endpoint.api.tm.teterin.ru/", className = "ru.teterin.tm.api.endpoint.RemoveUserResponse")
    public void removeUser(
        @WebParam(name = "session", targetNamespace = "")
            java.lang.String session,
        @WebParam(name = "userId", targetNamespace = "")
            java.lang.String userId
    ) throws Exception_Exception;
}

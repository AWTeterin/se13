package ru.teterin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    final SqlSessionFactory sqlSessionFactory;

    public TaskService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.sqlSessionFactory = serviceLocator.getSqlSessionService().getSqlSessionFactory();
    }

    @NotNull
    @Override
    public Task findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findOne(id);
        if (task == null) {
            session.close();
            throw new ObjectNotFoundException();
        }
        session.close();
        return task;
    }

    @NotNull
    @Override
    public Task findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findOneUserId(userId, id);
        if (task == null) {
            session.close();
            throw new ObjectNotFoundException();
        }
        session.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        @Nullable final List<Task> tasks = taskRepository.findAll();
        session.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        @Nullable final List<Task> tasks = taskRepository.findAllUserId(userId);
        session.close();
        return tasks;
    }

    @Override
    public void persist(
        @Nullable final Task task
    ) throws Exception {
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        try {
            taskRepository.persist(task);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new ObjectExistException();
        } finally {
            session.close();
        }
    }

    @Override
    public void persist(
        @Nullable final String userId,
        @Nullable final Task task
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String taskUserId = task.getUserId();
        if (!userId.equals(taskUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        try {
            taskRepository.persist(task);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new ObjectExistException();
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(
        @Nullable final Task task
    ) throws Exception {
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        try {
            taskRepository.merge(task);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(
        @Nullable final String userId,
        @Nullable final Task task
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String taskUserId = task.getUserId();
        if (!userId.equals(taskUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        try {
            taskRepository.merge(task);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        try {
            taskRepository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeUserId(userId, id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAllUserId(userId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public Task linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    ) throws Exception {
        final boolean noProjectId = projectId == null || projectId.isEmpty();
        final boolean noTaskId = id == null || id.isEmpty();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noProjectId || noTaskId || noUserId) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findOneUserId(userId, projectId);
        if (project == null) {
            throw new ObjectNotFoundException();
        }
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findOneUserId(userId, id);
        if (task == null) {
            throw new ObjectNotFoundException();
        }
        task.setProjectId(projectId);
        session.close();
        merge(task);
        return task;
    }

    @NotNull
    @Override
    public Collection<Task> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (sortOption == null) {
            sortOption = Constant.CREATE_ASC_SORT;
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> tasks;
        switch (sortOption) {
            default:
                tasks = taskRepository.sortByDateCreate(userId, Constant.ASC);
                break;
            case Constant.CREATE_DESC_SORT:
                tasks = taskRepository.sortByDateCreate(userId, Constant.DESC);
                break;
            case Constant.START_DATE_ASC_SORT:
                tasks = taskRepository.sortByDateStart(userId, Constant.ASC);
                break;
            case Constant.START_DATE_DESC_SORT:
                tasks = taskRepository.sortByDateStart(userId, Constant.DESC);
                break;
            case Constant.END_DATE_ASC_SORT:
                tasks = taskRepository.sortByDateEnd(userId, Constant.ASC);
                break;
            case Constant.END_DATE_DESC_SORT:
                tasks = taskRepository.sortByDateEnd(userId, Constant.DESC);
                break;
            case Constant.STATUS_ASC_SORT:
                tasks = taskRepository.sortByStatus(userId, Constant.ASC);
                break;
            case Constant.STATUS_DESC_SORT:
                tasks = taskRepository.sortByStatus(userId, Constant.DESC);
                break;
        }
        session.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (searchString == null) {
            searchString = "";
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> tasks = taskRepository.searchByString(userId, searchString);
        session.close();
        return tasks;
    }

}

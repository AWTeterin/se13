package ru.teterin.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IPropertyService;
import ru.teterin.tm.api.service.ISessionService;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.Session;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.util.AES;
import ru.teterin.tm.util.SignatureUtil;

import java.sql.SQLException;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.propertyService = serviceLocator.getPropertyService();
        this.sqlSessionFactory = serviceLocator.getSqlSessionService().getSqlSessionFactory();
    }

    @NotNull
    @Override
    public Session findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        @Nullable final Session session = sessionRepository.findOne(id);
        if (session == null) {
            sqlSession.close();
            throw new ObjectNotFoundException();
        }
        sqlSession.close();
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        @Nullable final List<Session> sessions = sessionRepository.findAll();
        sqlSession.close();
        return sessions;
    }

    @Override
    public void persist(
        @Nullable final Session session
    ) throws Exception {
        if (session == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.persist(session);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new ObjectExistException();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(
        @Nullable final Session session
    ) throws Exception {
        if (session == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.merge(session);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.remove(id);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    private Session open(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception {
        final boolean noLogin = login == null || login.isEmpty();
        if (noLogin) {
            throw new Exception(ExceptionConstant.EMPTY_LOGIN);
        }
        final boolean noPassword = password == null || password.isEmpty();
        if (noPassword) {
            throw new Exception(ExceptionConstant.EMPTY_PASSWORD);
        }
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new Exception(ExceptionConstant.OBJECT_NOT_FOUND);
        }
        @NotNull final String userPassword = user.getPassword();
        final boolean incorrectPassword = !password.equals(userPassword);
        if (incorrectPassword) {
            throw new Exception(ExceptionConstant.INCORRECT_PASSWORD);
        }
        @NotNull final Session session = new Session();
        @NotNull final String userId = user.getId();
        session.setUserId(userId);
        @Nullable final Role userRole = user.getRole();
        session.setRole(userRole);
        sign(session);
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.persist(session);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new ObjectExistException();
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Override
    public void validate(
        @Nullable final String session
    ) throws Exception {
        if (session == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final Session result = decryptSession(session);
        if (result == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final String signature = result.getSignature();
        final boolean noSignature = signature == null || signature.isEmpty();
        if (noSignature) {
            throw new AccessForbiddenException();
        }
        @Nullable final String userId = result.getUserId();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noUserId) {
            throw new AccessForbiddenException();
        }
        @NotNull final Session temp = result.clone();
        if (temp == null) {
            throw new AccessForbiddenException();
        }
        @NotNull final String signatureSource = result.getSignature();
        sign(temp);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean incorrectSignature = !signatureSource.equals(signatureTarget);
        if (incorrectSignature) {
            throw new AccessForbiddenException();
        }
        @NotNull final String sessionId = result.getId();
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        final boolean incorrectId = !sessionRepository.contains(sessionId);
        if (incorrectId) {
            throw new AccessForbiddenException();
        }
        sqlSession.close();
    }

    @Override
    public void validateWithRole(
        @Nullable final String session,
        @Nullable final Role role
    ) throws Exception {
        validate(session);
        @NotNull final Session temp = decryptSession(session);
        @NotNull final Role userRole = temp.getRole();
        final boolean accessDenied = !userRole.equals(role);
        if (accessDenied) {
            throw new AccessForbiddenException();
        }
    }

    @Override
    public void close(
        @Nullable final String session
    ) throws Exception {
        validate(session);
        @Nullable final Session temp = decryptSession(session);
        @NotNull final String sessionId = temp.getId();
        @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.remove(sessionId);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            sqlSession.close();
        }
    }

    private Session sign(
        @Nullable final Session session
    ) {
        if (session == null) {
            return null;
        }
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Nullable
    @Override
    public String getToken(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception {
        @Nullable final Session session = open(login, password);
        if (session == null) {
            return null;
        }
        @Nullable final String encryptedSession = encryptSession(session);
        return encryptedSession;
    }

    @Nullable
    private String encryptSession(
        @Nullable final Session session
    ) throws Exception {
        @NotNull final String key = propertyService.getSecretKey();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return AES.encrypt(json, key);
    }

    @Nullable
    private Session decryptSession(
        @Nullable final String session
    ) throws Exception {
        @NotNull final String key = propertyService.getSecretKey();
        @NotNull final String json = AES.decrypt(session, key);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Session result = mapper.readValue(json, Session.class);
        return result;
    }

    @NotNull
    @Override
    public String getUserId(
        @Nullable final String session
    ) throws Exception {
        if (session == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final Session temp = decryptSession(session);
        if (temp == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final String userId = temp.getUserId();
        if (userId == null) {
            throw new AccessForbiddenException();
        }
        return userId;
    }

}

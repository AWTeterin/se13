package ru.teterin.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.service.IDomainService;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.Domain;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.entity.User;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class DomainService implements IDomainService {

    public DomainService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        this.userService = serviceLocator.getUserService();
        this.projectService = serviceLocator.getProjectService();
        this.taskService = serviceLocator.getTaskService();
    }

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Override
    public void save(
        @Nullable Domain domain
    ) throws Exception {
        if (domain == null) {
            throw new Exception(ExceptionConstant.NO_DOMAIN);
        }
        domain.setUsers(userService.findAll());
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
    }

    @Override
    public void load(
        @Nullable Domain domain
    ) throws Exception {
        if (domain == null) {
            throw new Exception(ExceptionConstant.NO_DOMAIN);
        }
        @Nullable final Collection<User> users = domain.getUsers();
        if (users == null) {
            throw new Exception(ExceptionConstant.OBJECT_NOT_FOUND);
        }
        for (@NotNull final User user : users) {
            userService.persist(user);
        }
        @Nullable final Collection<Project> projects = domain.getProjects();
        if (projects == null) {
            throw new Exception(ExceptionConstant.NO_PROJECT);
        }
        for (@NotNull final Project project : projects) {
            projectService.persist(project);
        }
        @Nullable final Collection<Task> tasks = domain.getTasks();
        if (tasks == null) {
            throw new Exception(ExceptionConstant.NO_TASK);
        }
        for (@NotNull final Task task : tasks) {
            taskService.persist(task);
        }
    }

    @Override
    public void saveBinary() throws Exception {
        @NotNull final Path path = Paths.get(Constant.BIN_FILE);
        @NotNull final Domain domain = readData(path);
        try (
            @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(path.toFile());
            @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        ) {
            objectOutputStream.writeObject(domain);
        }
    }

    @Override
    public void loadBinary() throws Exception {
        @NotNull final Path path = Paths.get(Constant.BIN_FILE);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new Exception(ExceptionConstant.FILE_NOT_FOUND);
        }
        try (
            @NotNull final FileInputStream fileInputStream = new FileInputStream(path.toFile());
            @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            clearData();
            load(domain);
        } catch (ClassNotFoundException | IOException e) {
            throw new Exception(ExceptionConstant.INCORRECT_DATA_FILE);
        }
    }

    @Override
    public void saveJsonFasterxml() throws Exception {
        @NotNull final Path path = Paths.get(Constant.JSON_FILE);
        @NotNull final Domain domain = readData(path);
        @NotNull final ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
        writer.writeValue(path.toFile(), domain);
    }

    @Override
    public void saveJsonJaxb() throws Exception {
        @NotNull final Path path = Paths.get(Constant.JSON_FILE);
        @NotNull final Domain domain = readData(path);
        @NotNull final Map<String, Object> properties = new HashMap<String, Object>(2);
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Class[] clazz = new Class[]{Domain.class};
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(clazz, properties);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, path.toFile());
    }

    @Override
    public void loadJsonFasterxml() throws Exception {
        @NotNull final Path path = Paths.get(Constant.JSON_FILE);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new Exception(ExceptionConstant.FILE_NOT_FOUND);
        }
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final Domain domain = mapper.readValue(path.toFile(), Domain.class);
        clearData();
        load(domain);
    }

    @Override
    public void loadJsonJaxb() throws Exception {
        @NotNull final Path path = Paths.get(Constant.JSON_FILE);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new Exception(ExceptionConstant.FILE_NOT_FOUND);
        }
        @NotNull final Map<String, Object> properties = new HashMap<String, Object>(2);
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Class[] clazz = new Class[]{Domain.class};
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(clazz, properties);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        clearData();
        load(domain);
    }

    @Override
    public void saveXmlFasterxml() throws Exception {
        @NotNull final Path path = Paths.get(Constant.XML_FILE);
        @NotNull final Domain domain = readData(path);
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final ObjectWriter objectWriter = xmlMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(path.toFile(), domain);
    }

    @Override
    public void saveXmlJaxb() throws Exception {
        @NotNull final Path path = Paths.get(Constant.XML_FILE);
        @NotNull final Domain domain = readData(path);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, path.toFile());
    }

    @Override
    public void loadXmlFasterxml() throws Exception {
        @NotNull final Path path = Paths.get(Constant.XML_FILE);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new Exception(ExceptionConstant.FILE_NOT_FOUND);
        }
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Domain domain = mapper.readValue(path.toFile(), Domain.class);
        clearData();
        load(domain);
    }

    @Override
    public void loadXmlJaxb() throws Exception {
        @NotNull final Path path = Paths.get(Constant.XML_FILE);
        final boolean noFile = !Files.exists(path);
        if (noFile) {
            throw new Exception(ExceptionConstant.FILE_NOT_FOUND);
        }
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(path.toFile());
        clearData();
        load(domain);
    }

    @Override
    public void clearData() throws Exception {
        taskService.removeAll();
        projectService.removeAll();
        userService.removeAll();
    }

    @NotNull
    @Override
    public Domain readData(
        @NotNull final Path path
    ) throws Exception {
        final boolean hasFile = Files.exists(path);
        if (hasFile) {
            Files.deleteIfExists(path);
        }
        Files.createFile(path);
        @NotNull final Domain domain = new Domain();
        save(domain);
        return domain;
    }

}

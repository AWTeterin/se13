package ru.teterin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ProjectService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        sqlSessionFactory = serviceLocator.getSqlSessionService().getSqlSessionFactory();
    }

    @NotNull
    @Override
    public Project findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findOne(id);
        if (project == null) {
            session.close();
            throw new ObjectNotFoundException();
        }
        session.close();
        return project;
    }

    @NotNull
    @Override
    public Project findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        @Nullable final Project project = projectRepository.findOneUserId(userId, id);
        if (project == null) {
            session.close();
            throw new ObjectNotFoundException();
        }
        session.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        @Nullable final List<Project> projects = projectRepository.findAll();
        session.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        @Nullable final List<Project> projects = projectRepository.findAllUserId(userId);
        session.close();
        return projects;
    }

    @Override
    public void persist(
        @Nullable final Project project
    ) throws Exception {
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.persist(project);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new ObjectExistException();
        } finally {
            session.close();
        }
    }

    @Override
    public void persist(
        @Nullable final String userId,
        @Nullable final Project project
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String projectUserId = project.getUserId();
        if (!userId.equals(projectUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.persist(project);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new ObjectExistException();
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(
        @Nullable final Project project
    ) throws Exception {
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.merge(project);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(
        @Nullable final String userId,
        @Nullable final Project project
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String projectUserId = project.getUserId();
        if (!userId.equals(projectUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.merge(project);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeUserId(userId, id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeAllUserId(userId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        findOne(userId, id);
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> tasks = taskRepository.findAllByProjectId(userId, id);
        session.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Project> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (sortOption == null) {
            sortOption = Constant.CREATE_ASC_SORT;
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        @NotNull final Collection<Project> projects;
        switch (sortOption) {
            default:
                projects = projectRepository.sortByDateCreate(userId, Constant.ASC);
                break;
            case Constant.CREATE_DESC_SORT:
                projects = projectRepository.sortByDateCreate(userId, Constant.DESC);
                break;
            case Constant.START_DATE_ASC_SORT:
                projects = projectRepository.sortByDateStart(userId, Constant.ASC);
                break;
            case Constant.START_DATE_DESC_SORT:
                projects = projectRepository.sortByDateStart(userId, Constant.DESC);
                break;
            case Constant.END_DATE_ASC_SORT:
                projects = projectRepository.sortByDateEnd(userId, Constant.ASC);
                break;
            case Constant.END_DATE_DESC_SORT:
                projects = projectRepository.sortByDateEnd(userId, Constant.DESC);
                break;
            case Constant.STATUS_ASC_SORT:
                projects = projectRepository.sortByStatus(userId, Constant.ASC);
                break;
            case Constant.STATUS_DESC_SORT:
                projects = projectRepository.sortByStatus(userId, Constant.DESC);
                break;
        }
        session.close();
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (searchString == null) {
            searchString = "";
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
        @NotNull final Collection<Project> projects = projectRepository.searchByString(userId, searchString);
        session.close();
        return projects;
    }

}

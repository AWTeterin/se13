package ru.teterin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.exception.ObjectExistException;
import ru.teterin.tm.exception.ObjectNotFoundException;

import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public UserService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        sqlSessionFactory = serviceLocator.getSqlSessionService().getSqlSessionFactory();
    }

    @NotNull
    @Override
    public User findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        @Nullable final User user = userRepository.findOne(id);
        if (user == null) {
            session.close();
            throw new ObjectNotFoundException();
        }
        session.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        @Nullable final List<User> users = userRepository.findAll();
        session.close();
        return users;
    }

    @Override
    public void persist(
        @Nullable final User user
    ) throws Exception {
        if (user == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String password = user.getPassword();
        if (password == null || password.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        try {
            userRepository.persist(user);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new ObjectExistException();
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(
        @Nullable User user
    ) throws Exception {
        if (user == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        user = editForMerge(user);
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        try {
            userRepository.merge(user);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_UPDATE);
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        try {
            userRepository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        try {
            userRepository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        } finally {
            session.close();
        }
    }

    @Nullable
    @Override
    public User findByLogin(
        @Nullable final String login
    ) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_LOGIN);
        }
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        @Nullable final User user = userRepository.findByLogin(login);
        session.close();
        return user;
    }

    @NotNull
    private User editForMerge(
        @NotNull final User user
    ) throws Exception {
        @NotNull final SqlSession session = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
        @Nullable final String login = user.getLogin();
        @Nullable final User userBd = userRepository.findByLogin(login);
        session.close();
        @Nullable final String id = user.getUserId();
        if ("1".equals(id)) {
            userBd.setLogin(user.getPassword());
        }
        if ("2".equals(id)) {
            userBd.setPassword(user.getPassword());
        }
        return userBd;
    }

}

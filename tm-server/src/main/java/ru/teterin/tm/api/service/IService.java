package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<T extends AbstractEntity> {

    @NotNull
    public T findOne(
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public List<T> findAll() throws Exception;

    public void persist(
        @Nullable final T t
    ) throws Exception;

    public void merge(
        @Nullable final T t
    ) throws Exception;

    public void remove(
        @Nullable final String id
    ) throws Exception;

    public void removeAll() throws Exception;

}

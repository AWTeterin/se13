package ru.teterin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @Nullable
    @Select("SELECT * FROM app_task WHERE id = #{id}")
    public Task findOne(
        @NotNull @Param("id") final String id
    ) throws Exception;

    @Nullable
    @Select("SELECT * FROM app_task WHERE id = #{id} AND userId = #{userId}")
    public Task findOneUserId(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("id") final String id
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task")
    public List<Task> findAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId}")
    public List<Task> findAllUserId(
        @NotNull @Param("userId") final String userId
    ) throws Exception;

    @Insert("INSERT INTO app_task VALUES (#{id}, #{userId}, #{projectId}, #{name}, #{description}, #{dateCreate}, #{dateStart}, #{dateEnd}, #{status})")
    public void persist(
        @NotNull final Task task
    ) throws Exception;

    @Update("INSERT INTO app_task VALUES (#{id}, #{userId}, #{projectId}, #{name}, #{description}, #{dateCreate}, #{dateStart}, #{dateEnd}, #{status})" +
        "ON DUPLICATE KEY UPDATE projectId = #{projectId}, name = #{name}, description = #{description}, dateStart = #{dateStart}, dateEnd = #{dateEnd}, status = #{status}")
    public void merge(
        @NotNull final Task task
    ) throws Exception;

    @Delete("DELETE FROM app_task WHERE id = #{id}")
    public void remove(
        @NotNull @Param("id") final String id
    ) throws Exception;

    @Delete("DELETE FROM app_task WHERE id = #{id} AND userId = #{userId}")
    public void removeUserId(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("id") final String id
    ) throws Exception;

    @Delete("DELETE FROM app_task")
    public void removeAll() throws Exception;

    @Delete("DELETE FROM app_task WHERE userId = #{userId}")
    public void removeAllUserId(
        @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} " +
        "AND (name LIKE CONCAT('%',#{searchString},'%') OR description LIKE CONCAT('%',#{searchString},'%'))")
    public Collection<Task> searchByString(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("searchString") final String searchString
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY dateCreate")
    public Collection<Task> sortByDateCreate(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("sortType") final String sortType
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY dateStart")
    public Collection<Task> sortByDateStart(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("sortType") final String sortType
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY dateEnd")
    public Collection<Task> sortByDateEnd(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("sortType") final String sortType
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY status")
    public Collection<Task> sortByStatus(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("sortType") final String sortType
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} AND projectId = #{projectId}")
    public Collection<Task> findAllByProjectId(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("projectId") final String projectId
    ) throws Exception;

}
package ru.teterin.tm.api.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @Nullable
    @Select("SELECT * FROM app_user WHERE id = #{id}")
    public User findOne(
        @NotNull final String id
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_user")
    public List<User> findAll() throws Exception;

    @Insert("INSERT INTO app_user VALUES (#{id}, #{userId}, #{login}, #{password}, #{role})")
    public void persist(
        @NotNull final User user
    ) throws Exception;

    @Update("INSERT INTO app_user VALUES (#{id}, #{userId}, #{login}, #{password}, #{role} )" +
        "ON DUPLICATE KEY UPDATE login = #{login}, password = #{password}, role = #{role}")
    public void merge(
        @NotNull final User user
    ) throws Exception;

    @Delete("DELETE FROM app_user WHERE id = #{id}")
    public void remove(
        @NotNull final String id
    ) throws Exception;

    @Delete("DELETE FROM app_user")
    public void removeAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM app_user WHERE login = #{login}")
    public User findByLogin(
        @NotNull final String login
    ) throws Exception;

}

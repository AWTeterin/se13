package ru.teterin.tm.api.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    @Nullable
    @Select("SELECT * FROM app_session WHERE id = #{id}")
    public Session findOne(
        @NotNull final String id
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_session")
    public List<Session> findAll() throws Exception;

    @Insert("INSERT INTO app_session VALUES (#{id}, #{userId}, #{role}, #{timestamp}, #{signature})")
    public void persist(
        @NotNull final Session session
    ) throws Exception;

    @Update("INSERT INTO app_session VALUES (#{id}, #{userId}, #{role}, #{timestamp}, #{signature}) " +
        "ON DUPLICATE KEY UPDATE signature = #{signature}")
    public void merge(
        @NotNull final Session session
    ) throws Exception;

    @Delete("DELETE FROM app_session WHERE id = #{id}")
    public void remove(
        @NotNull final String id
    ) throws Exception;

    @Delete("DELETE FROM app_session")
    public void removeAll() throws Exception;

    @Select("SELECT EXISTS (SELECT id from app_session WHERE id = #{sessionId})")
    public boolean contains(
        @NotNull final String sessionId
    ) throws Exception;

}

package ru.teterin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    @Nullable
    @Select("SELECT * FROM app_project WHERE id = #{id}")
    public Project findOne(
        @NotNull @Param("id") final String id
    ) throws Exception;

    @Nullable
    @Select("SELECT * FROM app_project WHERE id = #{id} AND userId = #{userId}")
    public Project findOneUserId(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("id") final String id
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project")
    public List<Project> findAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId}")
    public List<Project> findAllUserId(
        @NotNull @Param("userId") final String userId
    ) throws Exception;

    @Insert("INSERT INTO app_project VALUES (#{id}, #{userId}, #{name}, #{description}, #{dateCreate}, #{dateStart}, #{dateEnd}, #{status})")
    public void persist(
        @NotNull final Project project
    ) throws Exception;

    @Update("INSERT INTO app_project VALUES (#{id}, #{userId}, #{name}, #{description}, #{dateCreate}, #{dateStart}, #{dateEnd}, #{status})" +
        "ON DUPLICATE KEY UPDATE name = #{name}, description = #{description}, dateStart = #{dateStart}, dateEnd = #{dateEnd}, status = #{status}")
    public void merge(
        @NotNull final Project project
    ) throws Exception;

    @Delete("DELETE FROM app_project WHERE id = #{id}")
    public void remove(
        @NotNull @Param("id") final String id
    ) throws Exception;

    @Delete("DELETE FROM app_project WHERE id = #{id} AND userId = #{userId}")
    public void removeUserId(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("id") final String id
    ) throws Exception;

    @Delete("DELETE FROM app_project")
    public void removeAll() throws Exception;

    @Delete("DELETE FROM app_project WHERE userId = #{userId}")
    public void removeAllUserId(
        @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} " +
        "AND (name LIKE CONCAT('%',#{searchString},'%') OR description LIKE CONCAT('%',#{searchString},'%'))")
    public Collection<Project> searchByString(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("searchString") final String searchString
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY dateCreate")
    public Collection<Project> sortByDateCreate(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("sortType") final String sortType
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY dateStart")
    public Collection<Project> sortByDateStart(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("sortType") final String sortType
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY dateEnd")
    public Collection<Project> sortByDateEnd(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("sortType") final String sortType
    ) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY status")
    public Collection<Project> sortByStatus(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("sortType") final String sortType
    ) throws Exception;

}

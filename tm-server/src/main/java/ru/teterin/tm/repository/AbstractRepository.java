package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IRepository;
import ru.teterin.tm.entity.AbstractEntity;

import java.sql.Connection;
import java.util.List;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(
        @NotNull final Connection connection
    ) {
        this.connection = connection;
    }

    @Nullable
    @Override
    public abstract T findOne(
        @NotNull final String id
    ) throws Exception;

    @NotNull
    @Override
    public abstract List<T> findAll() throws Exception;

    @Override
    public abstract void persist(
        @NotNull final T t
    ) throws Exception;

    @Override
    public abstract void merge(
        @NotNull final T t
    ) throws Exception;

    @Override
    public abstract void remove(
        @NotNull final String id
    ) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;

}

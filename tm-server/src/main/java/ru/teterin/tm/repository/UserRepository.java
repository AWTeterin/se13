package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.constant.FieldConstant;
import ru.teterin.tm.entity.User;
import ru.teterin.tm.enumerated.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(
        @NotNull final Connection connection
    ) {
        super(connection);
    }

    @Nullable
    @Override
    public User findOne(
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_user WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            resultSet.close();
            statement.close();
            return null;
        }
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        statement.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final String query = "SELECT * FROM app_user";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) {
            statement.close();
            resultSet.close();
            return Collections.emptyList();
        }
        List<User> users = new ArrayList<>();
        while (resultSet.next()) {
            users.add(fetch(resultSet));
        }
        statement.close();
        resultSet.close();
        return users;
    }

    @Override
    public void persist(
        @NotNull final User user
    ) throws Exception {
        @NotNull final String query = "INSERT INTO app_user VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getUserId());
        statement.setString(3, user.getLogin());
        statement.setString(4, user.getPassword());
        statement.setString(5, user.getRole().displayName());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void merge(
        @NotNull final User user
    ) throws Exception {
        @NotNull final String query = "INSERT INTO app_user VALUES (?, ?, ?, ?, ?) " +
            "ON DUPLICATE KEY UPDATE login = ?, password = ?, role = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getUserId());
        statement.setString(3, user.getLogin());
        statement.setString(4, user.getPassword());
        statement.setString(5, user.getRole().displayName());
        statement.setString(6, user.getLogin());
        statement.setString(7, user.getPassword());
        statement.setString(8, user.getRole().displayName());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void remove(
        @NotNull final String id
    ) throws Exception {
        @NotNull final String query = "DELETE FROM app_user WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "DELETE FROM app_user";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Nullable
    private User fetch(
        @Nullable final ResultSet row
    ) throws Exception {
        if (row == null) {
            return null;
        }
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConstant.ID));
        user.setUserId(row.getString(FieldConstant.USER_ID));
        user.setPassword(row.getString(FieldConstant.PASSWORD));
        user.setRole(Role.valueOf(row.getString(FieldConstant.ROLE)));
        user.setLogin(row.getString(FieldConstant.LOGIN));
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(
        @NotNull final String login
    ) throws Exception {
        @NotNull final String query = "SELECT * FROM app_user WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            resultSet.close();
            statement.close();
            return null;
        }
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        statement.close();
        return user;
    }

}

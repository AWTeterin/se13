package ru.teterin.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.enumerated.Role;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractEntity implements Serializable {

    public static final long serialVersionUID = 3;

    @NotNull
    protected String login = "";

    @NotNull
    private String password = "";

    @NotNull
    private Role role = Role.USER;

}

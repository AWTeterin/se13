package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.IProjectEndpoint;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @Override
    @WebMethod
    public void mergeProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        projectService.merge(userId, project);
    }

    @Override
    @WebMethod
    public void persistProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        projectService.persist(userId, project);
    }

    @NotNull
    @Override
    @WebMethod
    public Project findOneProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final Project project = projectService.findOne(userId, projectId);
        return project;
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProject(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final List<Project> projects = projectService.findAll(userId);
        return projects;
    }

    @Override
    @WebMethod
    public void removeProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        projectService.remove(userId, projectId);
    }

    @Override
    @WebMethod
    public void removeAllProjects(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        projectService.removeAll(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> searchProjectByString(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final Collection<Project> projects = projectService.searchByString(userId, string);
        return projects;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> sortAllProject(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final Collection<Project> projects = projectService.findAndSortAll(userId, sortOption);
        return projects;
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> findAllProjectTasks(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        validateSession(session);
        @Nullable final String userId = getUserId(session);
        @NotNull final Collection<Task> projects = projectService.findAllTaskByProjectId(userId, projectId);
        return projects;
    }

}
